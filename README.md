# food-camp-infra


This is a infrastructure repo for food-camp project

## Steps to run Terraform with GCP

- Install google cloud cli
- Install Terraform
- Authenticate to google cloud with gcloud cli like the following: `gcloud auth application-default login`
- Configure gcloud cli like this: `gcloud init`
- Initialize project directory for terraform `terraform init`
- Check the plan of deploying Terraform infrastructure `terraform plan`
- Finally, apply Terraform code for your gcp project `terraform apply`

## Consideration

- Check if gcloud cli is configured properly for your project
- Check login method to obtain neccessary permissions for resource creation
- Check Terraform variables in **variables.tf** files to point to proper project

## Description

- Created Terraform infrastructure for GCP.
- Implementation in progress...

## Author

- Krazhevskiy Aleksey
  - [github](https://github.com/alekseykrazhev)
  - [gitlab](https://gitlab.com/alekseykrazhev)
  - [dockerhub](https://hub.docker.com/u/alekseykrazhev)
