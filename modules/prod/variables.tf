variable "region_map" {
  type = map(string)
  default = {
    "europe-central2"   = "ec"
    "europe-southwest1" = "es"
  }
}
variable "env" {
}
variable "company" {
}
variable "var_private_subnet" {
}
variable "var_public_subnet" {
}
variable "var_region_name" {
}
variable "network_self_link" {
}
variable "subnetwork1" {
}