output "out_public_subnet_name" {
  description = "Name of public subnet from us-central-1"
  value       = google_compute_subnetwork.public_subnet.name
}

output "pub_address" {
  value = google_compute_subnetwork.public_subnet.gateway_address
}

output "pri_address" {
  value = google_compute_subnetwork.private_subnet.gateway_address
}