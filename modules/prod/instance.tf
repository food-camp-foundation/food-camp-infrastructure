resource "google_dns_record_set" "food_camp" {
  managed_zone = "food-camp-net"

  name    = "${var.env}.food-camp.net."
  type    = "A"
  # rrdatas = [google_compute_instance.default.network_interface[0].access_config[0].nat_ip]
  rrdatas = [google_compute_global_address.default.address]
  ttl     = 300
}

# resource "google_compute_instance" "default" {
#   name         = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-instance1")
#   machine_type = "e2-standard-2"
#   #zone         =   "${element(var.var_zones, count.index)}"
#   zone = format("%s", "${var.var_region_name}-a")
#   tags = ["ssh", "http", "https"]
#   boot_disk {
#     initialize_params {
#       image = "debian-12-bookworm-v20240415"
#     }
#   }

#   labels = {
#     webserver   = "true"
#     environment = "${var.env}"
#   }

#   metadata = {
#     ssh-keys = "ansible:${file("../ansible.pub")}"
#   }
#   network_interface {
#     subnetwork = google_compute_subnetwork.public_subnet.name
#     access_config {
#       // Ephemeral IP
#     }
#   }
# }


resource "google_compute_global_address" "default" {
  name = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-global-address")
}

resource "google_compute_http_health_check" "default" {
  name         = "http-health-check"
  request_path = "/"
  port         = "80"
}

resource "google_compute_backend_service" "default" {
  name          = "backend-service"
  health_checks = [google_compute_http_health_check.default.self_link]

  backend {
    group = google_compute_instance_group_manager.default.instance_group
  }
}

resource "google_compute_url_map" "default" {
  name            = "url-map"
  default_service = google_compute_backend_service.default.self_link
}

resource "google_compute_target_http_proxy" "default" {
  name    = "http-proxy"
  url_map = google_compute_url_map.default.self_link
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "global-forwarding-rule"
  target     = google_compute_target_http_proxy.default.self_link
  port_range = "80"
  ip_address = google_compute_global_address.default.address
}

resource "google_compute_autoscaler" "default" {
  provider = google-beta

  name   = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-autoscaler")
  zone   = format("%s", "${var.var_region_name}-a")
  target = google_compute_instance_group_manager.default.id

  autoscaling_policy {
    max_replicas    = 3
    min_replicas    = 1
    cooldown_period = 60

    # metric {
    #   name                       = "pubsub.googleapis.com/subscription/num_undelivered_messages"
    #   filter                     = "resource.type = pubsub_subscription AND resource.label.subscription_id = our-subscription"
    #   single_instance_assignment = 65535
    # }
    
    cpu_utilization {
      target = 0.5
    }
  }
}

resource "google_compute_instance_template" "default" {
  name           = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-instance-template")
  machine_type   = "e2-standard-2"
  region = "europe-central2"
  tags = ["ssh", "http", "https"]

  disk {
    source_image = "debian-cloud/debian-12"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    subnetwork = google_compute_subnetwork.public_subnet.name
    access_config {
      // Ephemeral public IP
    }
  }

  labels = {
    webserver   = "true"
    environment = "prod"
  }

  metadata = {
    ssh-keys = "ansible:${file("../ansible.pub")}"
  }
}

resource "google_compute_target_pool" "default" {
  provider = google-beta

  region = "europe-central2"

  name = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-target-pool")
}

resource "google_compute_instance_group_manager" "default" {
  provider = google-beta

  name = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-igm")
  zone = format("%s", "${var.var_region_name}-a")

  version {
    instance_template = google_compute_instance_template.default.id
    name              = "primary"
  }

  target_pools       = [google_compute_target_pool.default.id]
  base_instance_name = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-web-lb")
}

# data "google_compute_image" "debian_12" {
#   provider = google-beta

#   family  = "debian-12"
#   project = "debian-cloud"
# }

# provider "google-beta" {
#   region = "europe-central2"
#   zone   = "europe-central2-a"
# }