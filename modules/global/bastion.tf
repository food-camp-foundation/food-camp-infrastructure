resource "google_compute_address" "static" {
  region = var.region
  name   = "bastion-external"
}

resource "google_compute_instance" "default" {
  name         = "bastion-${google_compute_network.vpc.name}"
  machine_type = "e2-standard-2"
  #zone         =   "${element(var.var_zones, count.index)}"
  zone = format("%s", "${var.region}-a")
  tags = ["bastion"]
  boot_disk {
    initialize_params {
      image = "debian-12-bookworm-v20240415"
    }
  }

  metadata = {
    ssh-keys = "jenkins:${file("../ansible.pub")}"
  }

  labels = {
    webserver   = "true"
    environment = "bastion"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.public_subnet.name
    access_config {
      nat_ip = google_compute_address.static.address
    }
    network_ip = var.bastion_ip
  }
}