variable "project" {
}
variable "env" {
}
variable "company" {
}
variable "var_stg_private_subnet" {
}
variable "var_stg_public_subnet" {
}
variable "var_dev_private_subnet" {
}
variable "var_dev_public_subnet" {
}
variable "bastion_ip" {
  default = "10.26.5.2"
}
variable "var_bastion_subnet" {
  default = "10.26.5.0/24"
}
variable "region" {
  default = "europe-central2"
}