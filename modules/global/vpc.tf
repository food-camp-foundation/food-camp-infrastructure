resource "google_compute_network" "vpc" {
  name                    = format("%s", "${var.company}-${var.env}-vpc")
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}

resource "google_compute_firewall" "allow-internal" {
  name    = "${var.company}-fw-allow-internal"
  network = google_compute_network.vpc.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  source_ranges = [
    "${var.var_stg_private_subnet}",
    "${var.var_dev_private_subnet}",
    "${var.var_stg_public_subnet}",
    "${var.var_dev_public_subnet}"
  ]
}

resource "google_compute_firewall" "allow-http" {
  name    = "${var.company}-fw-allow-http"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [
    "0.0.0.0/0"
  ]

  target_tags = ["http"]
}

resource "google_compute_firewall" "allow-https" {
  name    = "${var.company}-fw-allow-https"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_ranges = [
    "0.0.0.0/0"
  ]

  target_tags = ["https"]
}

resource "google_compute_firewall" "allow-bastion" {
  name    = "${var.company}-fw-allow-bastion"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = [
    "${google_compute_address.static.address}/32"
  ]

  target_tags = ["ssh"]
}

resource "google_compute_firewall" "allow-to-bastion" {
  name    = "${var.company}-fw-allow-to-bastion"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = [
    "185.53.133.78/32",
    "34.16.225.125/32"
  ]

  target_tags = ["bastion"]
}