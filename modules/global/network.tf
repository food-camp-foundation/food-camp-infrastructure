resource "google_compute_subnetwork" "public_subnet" {
  name          = format("%s", "${var.company}-${var.env}-bastion-pub-net")
  ip_cidr_range = var.var_bastion_subnet
  network       = google_compute_network.vpc.self_link
  region        = var.region
}