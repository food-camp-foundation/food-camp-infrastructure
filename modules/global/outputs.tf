output "out_vpc_self_link" {
  value = google_compute_network.vpc.self_link
}
output "out_bastion_public_ip" {
  value = google_compute_address.static.address
}