resource "google_dns_record_set" "food_camp" {
  managed_zone = "food-camp-net"

  name    = "${var.env}.food-camp.net."
  type    = "A"
  rrdatas = [google_compute_instance.default.network_interface[0].access_config[0].nat_ip]
  ttl     = 300
}

resource "google_compute_instance" "default" {
  name         = format("%s", "${var.company}-${var.env}-${var.region_map["${var.var_region_name}"]}-instance1")
  machine_type = "e2-standard-2"
  #zone         =   "${element(var.var_zones, count.index)}"
  zone = format("%s", "${var.var_region_name}-a")
  tags = ["ssh", "http", "https"]
  boot_disk {
    initialize_params {
      image = "debian-12-bookworm-v20240415"
    }
  }

  labels = {
    webserver   = "true"
    environment = "${var.env}"
  }

  metadata = {
    ssh-keys = "ansible:${file("../ansible.pub")}"
  }
  network_interface {
    subnetwork = google_compute_subnetwork.public_subnet.name
    access_config {
      // Ephemeral IP
    }
  }
}