provider "google" {
  credentials = file("gcp_key.json")
  project     = var.var_project
  # region      = "europe-central2-a"
}

provider "google-beta" {
  credentials = file("gcp_key.json")
  project     = var.var_project
  region = "europe-central2"
  zone   = "europe-central2-a"
}

module "vpc" {
  source                 = "../modules/global"
  env                    = "devstg"
  company                = var.var_company
  project                = var.var_project
  var_stg_public_subnet  = var.stg_public_subnet
  var_stg_private_subnet = var.stg_private_subnet
  var_dev_public_subnet  = var.dev_public_subnet
  var_dev_private_subnet = var.dev_private_subnet
}

module "stg" {
  source             = "../modules/envs"
  network_self_link  = module.vpc.out_vpc_self_link
  subnetwork1        = module.stg.out_public_subnet_name
  env                = "stg"
  company            = var.var_company
  var_public_subnet  = var.stg_public_subnet
  var_private_subnet = var.stg_private_subnet
  var_region_name    = var.var_stg_region_name
}

module "dev" {
  source             = "../modules/envs"
  network_self_link  = module.vpc.out_vpc_self_link
  subnetwork1        = module.dev.out_public_subnet_name
  env                = "dev"
  company            = var.var_company
  var_public_subnet  = var.dev_public_subnet
  var_private_subnet = var.dev_private_subnet
  var_region_name    = var.var_dev_region_name
}

module "prod" {
  source             = "../modules/prod"
  network_self_link  = module.vpc.out_vpc_self_link
  subnetwork1        = module.dev.out_public_subnet_name
  env                = "prod"
  company            = var.var_company
  var_public_subnet  = "10.26.6.0/24"
  var_private_subnet = "10.26.7.0/24"
  var_region_name    = "europe-central2"
}

# module "jenkins-server" {
#   source = "../modules/single-instance-server"
#   project  = "${var.var_project}"
#   env                   = "${var.var_env}"
#   company               = "${var.var_company}"
#   var_public_subnet = "${var.dev_public_subnet}"
#   var_private_subnet= "${var.dev_private_subnet}"
#   var_region_name = "${var.var_dev_region_name}"
#   network_self_link     = "${module.vpc.out_vpc_self_link}"
#   subnetwork1           = "${module.stg.out_public_subnet_name}"
#   instance_name = "${var.var_instance_name}"
#   machine_image = "${var.var_machine_image}"
# }

######################################################################
# Display Output Public Instance
######################################################################
output "stg_public_address" { value = module.stg.pub_address }
output "stg_private_address" { value = module.stg.pri_address }
output "dev_public_address" { value = module.dev.pub_address }
output "dev_private_address" { value = module.dev.pri_address }
output "vpc_self_link" { value = module.vpc.out_vpc_self_link }