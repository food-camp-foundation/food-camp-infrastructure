variable "var_project" {
  default = "ordinal-shield-421812"
}
# variable "var_env" {
#         default = "dev"
#     }
variable "var_company" {
  default = "food-camp"
}
variable "stg_private_subnet" {
  default = "10.26.1.0/24"
}
variable "stg_public_subnet" {
  default = "10.26.2.0/24"
}
variable "dev_private_subnet" {
  default = "10.26.3.0/24"
}
variable "dev_public_subnet" {
  default = "10.26.4.0/24"
}
variable "var_stg_region_name" {
  default = "europe-southwest1"
}
variable "var_dev_region_name" {
  default = "europe-central2"
}
variable "var_machine_image" {
  default = "jenkins-server"
}
variable "var_instance_name" {
  default = "jenkins-server"
}