terraform {
  backend "gcs" {
    bucket      = "tf-state-food-camp"
    prefix      = "terraform/state"
    credentials = "gcp_key.json"
  }
}